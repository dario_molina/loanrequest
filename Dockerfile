FROM python:3.6-alpine

RUN apk update && apk add openrc postgresql-dev jpeg-dev zlib-dev gcc musl-dev libxml2-dev libxslt-dev nfs-utils
RUN rc-update add nfsmount default
ENV PYTHONUNBUFFERED 1

WORKDIR pythonbase/
COPY . pythonbase/
RUN ["pip", "install", "-r", "pythonbase/requirements.txt"]
EXPOSE 8000
