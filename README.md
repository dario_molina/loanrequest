# Loan Test
Peque�a aplicaci�n de prueba, en donde se consume una api que eval�a si una persona "x" puede obtener unprestamo o no. 

#### 1. Instalar python3.6
    $ sudo apt-get install python3.6-dev

#### 2. Instalar postgres
    $ sudo apt-get install libpq-dev
    $ sudo apt-get install postgresql

#### 3. Seleccionar alguno de los entornos virtuales ([virtualen](https://virtualenv.pypa.io/en/stable/) o [virtualenwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/)) e instalarlos

#### 4. Crear el entorno virtual
- Para viertualenv:
    ```
    $ virtualenv -p python3.6 scoring
    ```
- Para virtualenviwrapper:
    ```
    $ mkvirtualenv --python=python3 scoring
    ```

#### 5. Activar el entorno virtual
- Si se usa virtualenvwrapper
    ```
    $ workon scoring
    ```

#### 6. Ubicarnos en la carpeta principal y corre el siguiente comando:
    $ pip install requirements.txt
    
#### 7. Para Crear la base de datos postgres, usuario y password:
    $ su - postgres
    $ createuser scoring
    $ createdb scoring
    $ psql scoring
    $ ALTER USER scoring WITH PASSWORD 'scoring'
    $ ALTER ROLE scoring SET client_encoding TO 'utf8';
    $ ALTER ROLE scoring SET default_transaction_isolation TO 'read committed';
    $ ALTER ROLE scoring SET timezone TO 'UTC';
    $ GRANT ALL PRIVILEGES ON DATABASE scoring TO scoring;
    $ ALTER USER scoring CREATEDB;
    
#### 8. Correr las migraciones:
    $ python manage.py migrate
    
#### 9. Correr el servidor
    $ python manage.py runserver

## Para usarlo con docker-compose
    
#### 1. buildear
    $ docker-compose build

#### 2. up services
    $ docker-compose up

