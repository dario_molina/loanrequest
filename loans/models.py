from django.db import models
from django_extensions.db.models import TimeStampedModel
from model_utils import Choices


GENDER_CHOICES = Choices(
        ('M', 'Masculino'),
        ('F', 'Femenino'),
)


class LoanRequest(TimeStampedModel):
    dni = models.PositiveIntegerField()
    name = models.CharField(max_length=100)
    gender = models.CharField(choices=GENDER_CHOICES, max_length=10)
    email = models.EmailField(max_length=250)
    requested_amount = models.DecimalField(max_digits=10, decimal_places=2)

    def __str__(self):
        return '{} - {} - {}'.format(
            self.created,
            self.name,
            self.dni
        )
