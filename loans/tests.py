import unittest

from django.test import Client


class SimpleTest(unittest.TestCase):

    def test_status_code(self):
        client = Client()
        response = client.get('/loan_request_view/')
        self.assertEqual(response.status_code, 200)
