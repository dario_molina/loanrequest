from django.contrib import admin

from loans.models import LoanRequest


@admin.register(LoanRequest)
class LoanRequestModelAdmin(admin.ModelAdmin):
    pass
