from django.contrib.admin.views.decorators import staff_member_required
from django.db import transaction
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse_lazy
from django.views.generic import FormView, ListView, UpdateView, DeleteView
import json

from loans.forms import LoanRequestForm
from loans.models import LoanRequest
from loans.utils import ConsumerScoringService


class LoanRequestView(FormView):
    template_name = 'loans/register_request_form.html'
    form_class = LoanRequestForm
    success_url = reverse_lazy('loan_request_view')

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            loan_request = ConsumerScoringService(
                document_number=form.data['dni'],
                gender=form.data['gender'],
                email=form.data['email']
            ).get_content_url()
            if loan_request.get('approved'):
                form.save()
                status = 'APPROVED REQUEST'
            else:
                status = 'DISAPPROVED'
            if loan_request.get('error'):
                status = 'THERE WAS AN ERROR IN THE REQUEST'
            form = self.form_class()
            return render(request, self.template_name, {'form': form, 'is_approved': status})


class ListLoanRequestView(ListView):
    template_name = 'loans/loan_request_list.html'
    model = LoanRequest


class UpdateLoanRequestView(UpdateView):
    template_name = 'loans/register_request_form.html'
    model = LoanRequest
    form_class = LoanRequestForm
    success_url = reverse_lazy('loan_request_view')

    def form_valid(self, form):
        data = form.cleaned_data
        with transaction.atomic():
            LoanRequest.objects.filter(id=self.get_object().id).update(**data)
        return super(UpdateLoanRequestView, self).form_valid(form)


class DeleteLoanRequestView(DeleteView):
    model = LoanRequest
    success_url = reverse_lazy('list_loan_request_view')


loan_request_view = LoanRequestView.as_view()
list_loan_request_view = staff_member_required(ListLoanRequestView.as_view())
update_loan_request_view = staff_member_required(UpdateLoanRequestView.as_view())
delete_loan_request_view = staff_member_required(DeleteLoanRequestView.as_view())
