from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.loan_request_view, name='loan_request_view'),
    url(r'^list/$', views.list_loan_request_view, name='list_loan_request_view'),
    url(r'^update/(?P<pk>\d+)/$', views.update_loan_request_view, name='update_loan_request_view'),
    url(r'^delete/(?P<pk>\d+)/$', views.delete_loan_request_view, name='delete_loan_request_view')
]
