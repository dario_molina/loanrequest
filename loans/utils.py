from requests import get


class ConsumerScoringService(object):

    url = 'http://scoringservice.moni.com.ar:7001/api/v1/scoring/?document_number={}&gender={}&email={}'

    def __init__(self, document_number, gender, email):
        self.document_number = document_number
        self.gender = gender
        self.email = email

    def make_url(self, **kwargs):
        return self.url.format(self.document_number, self.gender, self.email)

    def get_content_url(self):
        response = get(self.make_url()).json()
        return response
