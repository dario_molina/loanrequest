from django.apps import AppConfig


class CurrencyTrackingConfig(AppConfig):
    name = 'loans'
