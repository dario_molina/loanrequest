from django import forms
from django.core.validators import MinValueValidator
from django_select2.forms import Select2Widget

from loans.models import LoanRequest, GENDER_CHOICES
from loans.utils import ConsumerScoringService


class LoanRequestForm(forms.ModelForm):
    gender = forms.ChoiceField(
        label='Gender',
        widget=Select2Widget,
        required=True,
        choices=GENDER_CHOICES,
        initial=GENDER_CHOICES.M
    )
    name = forms.CharField(
        max_length=100,
        required=True,
    )
    dni = forms.IntegerField(
        label='DNI',
        required=True,
        min_value=0,
        max_value=99999999,
    )
    email = forms.EmailField(
        required=True,
        max_length=250
    )
    requested_amount = forms.DecimalField(
        min_value=0.01,
        max_value=1000000,
        required=True
    )

    class Meta:
        model = LoanRequest
        fields = ['dni', 'name', 'gender', 'email', 'requested_amount']
